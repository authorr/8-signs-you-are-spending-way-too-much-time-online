# 8 Signs You Are Spending Way Too Much Time Online 

For obvious reasons, internet usage is spiking at the moment. However, even when we are not all stuck at home waiting out a pandemic, many of us are guilty of spending too much time online. 